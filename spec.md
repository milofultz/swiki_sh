# {{SWIKI}}.sh Spec

## Special Delimiters

Anything surrounded by double curly braces (e.g. `{{ title }}`) has special meaning. In the frame file, anything surrounded by double curly braces will be replaced. In any of the wiki entries, the curly braces will be replaced by links to the local files.

The spaces separating the double curly braces are optional.

## Input Folder

All files or folders with a preceding underscore will be ignored. e.g. `_ignore.md` and all files within the `_ignore/` folder would be ignored.

All non-markdown files not preceded by an underscore will be copied into the output folder.

### Wiki Entries

Each wiki entry is a markdown file that is not preceded by an underscore.

There is an optional, but recommended, YAML front matter that contains a `title` and/or a `description`.

The rest of the file is standard markdown.

The final HTML file's filename comes from the `title` converted to [all-lowercase kebab case](https://en.wikipedia.org/wiki/Letter_case#Kebab_case), or the filename if `title` is not present. The markdown's full filepath, title, and description will be used when listing your page in the index file.

#### Internal Links

Internal links use the `title` of another wiki entry surrounded by double curly braces (e.g. `{{ title }}`). If you want the link text to be different than the `title`, you precede the `title` with the link text and a pipe to separate them (e.g. `{{ link text | title }}`).

In the output, any wiki entry referred to via internal links will have a list of all wiki entries that linked to it, called "backlinks".

### `_swiki` folder

The input folder must have a `_swiki` subfolder within it.

The `_swiki` subfolder must contain a `frame.html` file. This file is a standard HTML file with multiple keywords that will be replaced if the corresponding data is present in the markdown file that is filling it.

* `{{ title }}` - The title property from the YAML front matter
* `{{ description }}` - The description property from the YAML front matter
* `{{ content }}` - The content of the Markdown file, less the front matter
* `{{ sitemap }}` - The sitemap of the wiki, which contains a list of each folder's entries

The `_swiki` subfolder must also contain an `index.md` file. This file is the same format as all other markdown files that are present and will be what fills the final output's `index.html` file. The title for the index file will always be replaced with "Index".

All other files in the `_swiki` subfolder will be copied into the output folder.

## Output Folder

All non-system files (e.g. files not preceded by a period) in the output folder will be deleted before newly converted and copied files are placed in the output folder.

All converted and copied input files will be moved to the root of the output folder, discarding any folder structure present in the input.

