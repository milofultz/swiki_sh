#content of the Markdown file, less the front matter!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/fill_frame.sh"
. "${BASH_SOURCE[0]%/*}/save_backlinks.sh"
. "${BASH_SOURCE[0]%/*}/save_metadata.sh"
. "${BASH_SOURCE[0]%/*}/save_wiki_entry_content.sh"

#
# @param {String} $1:relative_path Relative path to the current folder
# @returns {String} HTML of path section
#
build_path_section() {
    relative_path="$1"
    # add start tags of details, summary, ul to content
    # for file in folder
        # set href to file
        # set title to 1p
        # set description to 2p
        # add list item with anchor tag HTML string to content
    # add end tags of ul, summary, details to content
    echo "SECTION FOR $relative_path"
}

#
# @param {String} $1: input_dir Path to input directory
# @returns {String} HTML of list of folders
#
create_sitemap() {
    local HTML_SITEMAP_START="<details><summary>${1//\./\/}</summary><ul>"
    local HTML_SITEMAP_END="</ul></details>"

    local entries=""
    local subfolders=""
    local output=""

    # for folder in input dir
    for item in "$METADATA_PATH/$1"/*; do
        if [ -d "$item" ]; then
            subfolders="$folders\n$item"
        else
            href="$(basename "$item")"
            title="$(sed -ne '1p' "$item")"
            description="$(sed -ne '2p' "$item")"
            entries="$entries\n<li><a href=\"$href.html\">$title</a> - $description</li>"
        fi
    done

    output="${HTML_SITEMAP_START}${entries}${HTML_SITEMAP_END}"

    if [ -z "$subfolders" ]; then
        echo "$output"
        exit 0
    else
        echo "$subfolders" | while read -r line; do
            dir_name="${item##*$METADATA_PATH/}"
            output="$(create_sitemap "$dir_name")"
        done

        echo "$output"
    fi
}

#
# @param {String} $1:path_to_index Path to the index.md file
# @returns {String} HTML of filled index frame
#
create_index() {
    path_to_index="$1"

    # Ensure title of index is "Index"
    sed -i.bak -e 's/^title:.*$/title: Index/' "$path_to_index"

    # save metadata of index file
    path_to_metadata="$(save_metadata "$path_to_index")"
    # save content of index file
    path_to_content="$(save_wiki_entry_content "$path_to_index")"
    # save backlinks of index file
    backlinks="$(save_backlinks "$path_to_index")"

    frame="$(dirname "$path_to_index")/frame.html"
    title="$(sed -ne '1p' "$path_to_metadata")"
    # fill frame of index
    filled_frame_no_sitemap="$(fill_frame "$frame" "$title")"
    sitemap="$(create_sitemap ".")"
    echo "$filled_frame_no_sitemap" | sed -e "s|{{sitemap}}|$sitemap|g"
}
