export BACKLINK_ALL_RE='/{{ *([^|]*\|[^}]*)|([^}]*) *}}/'
export BACKLINK_LABELED_RE='/{{ *[^}|]*\|[^}]* *}}/'
export BACKLINK_UNLABELED_RE='/{{ *[^}]* *}}/'
export TRIM_SPACE_RE='/^ *| *$/'
