* ~~Replaces internal link with anchor tag to kebab name~~
* ~~Save converted content into "content_db" as `Title`~~
    * ~~Convert to HTML~~
    * ~~Save to db~~
* ~~Save all backlink titles in a given wiki entry to "backlink_db" folder as `Title` to later be fleshed out~~
    * ~~Get all backlink titles in converted HTML~~
    * ~~Save all to files in db per given linked-to file~~
* ~~Save front matter detail with kebab name and full path into "meta_db" folder as `Title` for filling out internal links~~
    * ~~Get filepath without starting input folder~~
    * ~~Get title and description from YAML front matter~~
    * ~~If no title, title = filename without full path~~
    * ~~Get kebab name~~
    * ~~Save file containing kebab name, full input path starting from input folder, and description in db~~
* ~~Convert backlinks file into links (or maybe make them links before putting them in the backlinks file? could be easier)~~
* ~~Fills frame with items from meta_db, content_db, and backlink_db~~
* Create final HTML file from wiki entry
* Create HTML files from all files in input folder
* Create index HTML from index.md and frame, as well as all input files

# META

* Rename prepare_wiki_entry_content to just prepare_content
* Rename pwec folder to just test_files or something
* Get as many things to use the same test data as possible
