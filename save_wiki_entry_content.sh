#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/init.sh"
. "${BASH_SOURCE[0]%/*}/prepare_wiki_entry_content.sh"
. "${BASH_SOURCE[0]%/*}/kebabify.sh"
. "${BASH_SOURCE[0]%/*}/get_metadata.sh"

#
# @param {String} $1:filepath Wiki entry to save in content folder
# @returns {String} destination_filepath Path to saved wiki entry content
#
save_wiki_entry_content() {
    filepath="$1"
    # TODO: this should be done after the front matter is parsed and saved
    title="$(get_metadata "$filepath" | sed -ne '1p')"
    title="$(kebabify "$title")"
    destination_filepath="$CONTENT_PATH/$title"
    prepare_wiki_entry_content "$filepath" > "$destination_filepath"
    echo "$destination_filepath"
}

