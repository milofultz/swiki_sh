#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/init.sh"
. "${BASH_SOURCE[0]%/*}/regex.sh"
. "${BASH_SOURCE[0]%/*}/kebabify.sh"

#
# @param {String} $1:filepath Wiki entry to save backlinks from
# @returns {String} Path to list of newline-separated kebabified pages that were
# linked to. The reason we are doing this and not a path to the file, is that
# we are writing to many *different* files, not just one like in the other
# `save_` functions.
#
save_backlinks() {
    filepath="$1"

    title="$(basename "$filepath")"
    title="${title%\.*}"
    backlinks="$(awk $"
        function printInternalLinks(line) {
            while (match(line, $BACKLINK_LABELED_RE)) {
                entireLink = substr(line, RSTART, RLENGTH);
                linkContent = substr(line, RSTART+2, RLENGTH-4);
                match(linkContent, /\\|.*/);
                title = substr(linkContent, RSTART+1, RLENGTH-1);
                gsub($TRIM_SPACE_RE, \"\", title);
                print title;
                gsub(entireLink, \"\", line);
            }
            while (match(line, $BACKLINK_UNLABELED_RE)) {
                entireLink = substr(line, RSTART, RLENGTH);
                title = substr(line, RSTART+2, RLENGTH-4);
                gsub($TRIM_SPACE_RE, \"\", title);
                print title;
                gsub(entireLink, \"\", line);
            }
        }

        $BACKLINK_ALL_RE {
            printInternalLinks(\$0);
        }
    " "$filepath")"

    if [ -z "$backlinks" ]; then
        exit 0
    fi

    temp_file="/tmp/tempfile"
    rm -f "$temp_file"

    link="<li><a href=\"$(kebabify "$title").html\">$title</a></li>"

    echo "$backlinks" | while read -r line; do
        destination_filename="$(kebabify "$line")"
        echo "$link" >> "$BACKLINKS_PATH/$destination_filename"
        echo "$destination_filename" >> "$temp_file"
    done

    cat "$temp_file"
    rm -f "$temp_file"
}

