#!/bin/bash

. "${BASH_SOURCE[0]%/*}/regex.sh"

#
# @param {String} $1:filepath Filepath of the file to have replacements
# @returns {String} Contents of the file with internal links replaced by
# anchor tags.
#
replace_backlinks() {
    filepath="$1"
    awk $"
        $(cat "${BASH_SOURCE[0]%/*}/awk/kebabify.awk")

        function formatInternalLinks(line) {
            while (match(line, $BACKLINK_LABELED_RE)) {
                entireLink = substr(line, RSTART, RLENGTH);
                linkContent = substr(line, RSTART+2, RLENGTH-4);
                match(linkContent, /^[^|]*/);
                label = substr(linkContent, RSTART, RLENGTH);
                gsub($TRIM_SPACE_RE, \"\", label);
                match(linkContent, /\\|.*/);
                href = substr(linkContent, RSTART+1, RLENGTH-1);
                href = kebabify(href);
                gsub($TRIM_SPACE_RE, \"\", href);
                newTag = \"<a href=\\\"\" href \".html\\\">\" label \"</a>\";
                sub(/\|/, \"\\\|\", entireLink); # Ensure whole link is replaced
                sub(entireLink, newTag, line);
            }
            while (match(line, $BACKLINK_UNLABELED_RE)) {
                entireLink = substr(line, RSTART, RLENGTH);
                label = substr(line, RSTART+2, RLENGTH-4);
                gsub(/^ *| *$/, \"\", label);
                href = kebabify(label);
                newTag = \"<a href=\\\"\" href \".html\\\">\" label \"</a>\";
                sub(entireLink, newTag, line);
            }
            return line;
        }

        $BACKLINK_ALL_RE {
            print formatInternalLinks(\$0);
            next;
        }

        { print; }
    " "$filepath"
}

