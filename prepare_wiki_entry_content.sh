#!/bin/bash

set -euo pipefail

#
# @param {String} $1:filepath Input file to be parsed
# @returns {String} Wiki entry content as HTML
#
prepare_wiki_entry_content() {
    filepath="$1"
    pandoc -f gfm -t html --no-highlight "$filepath"
}

