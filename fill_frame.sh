#!/bin/bash

. "${BASH_SOURCE[0]%/*}/init.sh"

#
# @param {String} $1:frame_file Path to the frame that will be filled
# @param {String} $2:title Titles of the files to use when filling the frame
# @returns {String} Filled HTML frame
#
fill_frame() {
    frame_file="$1"
    title="$2"
    filename="$(kebabify "$title")"

    metadata="$(cat "$METADATA_PATH/$filename")"
    title="$(echo "$metadata" | sed -ne '1p' | tr -d "\n")"
    description="$(echo "$metadata" | sed -ne '2p' | tr -d "\n")"

    filled_no_content="$(sed -e 's|{{ *|{{|g' -e 's| *}}|}}|g' -e "s|{{title}}|$title|g; s|{{description}}|$description|g;" "$frame_file")"

    content="$(cat "$CONTENT_PATH/$filename")"
    backlinks=""
    if [ -e "$BACKLINKS_PATH/$filename" ]; then
        backlinks="$(cat "$BACKLINKS_PATH/$filename")"
    fi
    backlinks_content=""
    if [ -n "$backlinks" ]; then
        backlinks_content="\n<h2>Backlinks</h2><ul>$backlinks</ul>"
    fi

    complete_content="${content}${backlinks_content}"

    echo -e "${filled_no_content%%\{\{content*}$complete_content${filled_no_content##*content\}\}}"
}

