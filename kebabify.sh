#!/bin/bash

set -euo pipefail

#
# @param {String} $1:input Input string to convert
# @returns {String} Kebabified version of input string
#
kebabify() {
    input="$1"
    echo "$input" | \
        awk "
            $(cat "${BASH_SOURCE[0]%/*}/awk/kebabify.awk")
            { print kebabify(\$0); }
        "
}

