#!/bin/bash

dir=${BASH_SOURCE[0]%/*}

set -euo pipefail

. "$dir/../get_metadata.sh"

dir=${BASH_SOURCE[0]%/*}

fails=0
i=0

# It should parse the metadata from the markdown file
for file in "$dir"/prepare_wiki_entry_content/*.md; do
    i=$((i + 1))
    expected="$(cat "${file%\.*}.meta")"
    actual="$(get_metadata "$file" "")"
    if test "$expected" != "$actual"; then
        fails=$((fails + 1))
        echo "$file: Failed"
        echo "EXPECTED OUTPUT >>>"
        echo "$expected"
        echo "<<<"
        echo "ACTUAL OUTPUT >>>"
        echo "$actual"
        echo "<<<"
    fi
done

exit "$fails"

