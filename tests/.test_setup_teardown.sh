#!/bin/bash

. "${BASH_SOURCE[0]%/*}/../init.sh"

set_up() {
    mkdir -p "$CONTENT_PATH"
    mkdir -p "$BACKLINKS_PATH"
    mkdir -p "$METADATA_PATH"
}

tear_down() {
    rm -rf "$CONTENT_PATH"
    rm -rf "$BACKLINKS_PATH"
    rm -rf "$METADATA_PATH"
}

