#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/.test_setup_teardown.sh"
. "${BASH_SOURCE[0]%/*}/../init.sh"
. "${BASH_SOURCE[0]%/*}/../fill_frame.sh"
. "${BASH_SOURCE[0]%/*}/../kebabify.sh"

fails=0

frame_file="${BASH_SOURCE[0]%/*}/fill_frame/.frame.html"

for file in "${BASH_SOURCE[0]%/*}"/fill_frame/*.html; do
    set_up

    name="$(basename "$file")"
    title="${name%\.*}"
    input_path="${file%/*}"
    dest_filename="$(kebabify "$title")"

    cp "$input_path/$title.html" "$CONTENT_PATH/$dest_filename"
    cp "$input_path/$title.backlinks" "$BACKLINKS_PATH/$dest_filename"
    cp "$input_path/$title.meta" "$METADATA_PATH/$dest_filename"

    expected="$(cat "$input_path/$title.parsed")"
    actual="$(fill_frame "$frame_file" "$title")"
    if test "$expected" != "$actual"; then
        fails=$((fails + 1))
        echo "$file: Failed"
        echo "EXPECTED OUTPUT >>>"
        echo "$expected"
        echo "<<<"
        echo "ACTUAL OUTPUT >>>"
        echo "$actual"
        echo "<<<"
    fi

    tear_down
done

exit "$fails"

