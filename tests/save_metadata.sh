#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/.test_setup_teardown.sh"
. "${BASH_SOURCE[0]%/*}/../kebabify.sh"
. "${BASH_SOURCE[0]%/*}/../save_metadata.sh"

fails=0

input_dir="${BASH_SOURCE[0]%/*}/prepare_wiki_entry_content"
folders=("" "Folder" "Existing/Folder")

for folder in "${folders[@]}"; do
    set_up

    if test "$folder" != ""; then
        clean_up=1
        input_folder="$input_dir/$folder"
        mkdir -p "$input_folder"
        cp "$input_dir"/*.* "$input_folder"
    else
        clean_up=0
        input_folder="$input_dir"
    fi

    for file in "$input_folder"/*.md; do
        filename="$(basename "$file")"
        path="$(realpath "$file")"
        title="$(head -n1 "${path%%\.*}.title")"
        output_filename="$(kebabify "${title%\.*}")"
        if test "$folder" != ""; then
            expected_destination="$METADATA_PATH/$folder/$output_filename"
        else
            expected_destination="$METADATA_PATH/$output_filename"
        fi
        actual_destination="$(save_metadata "$file" "$input_dir")"
        if [ "$expected_destination" != "$actual_destination" ]; then
            fails=$((fails + 1))
            echo "$file: Destination file '$actual_destination' doesn't match expected '$expected_destination'."
        elif [ ! -e "$actual_destination" ]; then
            fails=$((fails + 1))
            echo "$file: Destination file '$actual_destination' not found"
        fi
    done

    if test $clean_up == 1; then
        rm -rf "$input_folder"
    fi

    tear_down
done

exit "$fails"

