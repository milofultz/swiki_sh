#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/.test_setup_teardown.sh"
. "${BASH_SOURCE[0]%/*}/../init.sh"
. "${BASH_SOURCE[0]%/*}/../create_index.sh"

fails=0

index_file="${BASH_SOURCE[0]%/*}/create_index/_swiki/index.md"
frame_file="${BASH_SOURCE[0]%/*}/create_index/_swiki/frame.html"

set_up

for file in "${BASH_SOURCE[0]%/*}"/create_index/*.html; do
    name="$(basename "$file")"
    title="${name%\.*}"
    input_path="${file%/*}"
    dest_filename="$title"

    cp "$input_path/$title.html" "$CONTENT_PATH/$dest_filename"
    cp "$input_path/$title.backlinks" "$BACKLINKS_PATH/$dest_filename"
    cp "$input_path/$title.meta" "$METADATA_PATH/$dest_filename"
done

expected="$(cat "${BASH_SOURCE[0]%/*}/create_index/index.parsed")"
actual="$(create_index "$index_file")"
if test "$expected" != "$actual"; then
    fails=$((fails + 1))
    echo "$file: Failed"
    echo "EXPECTED OUTPUT >>>"
    echo "$expected"
    echo "<<<"
    echo "ACTUAL OUTPUT >>>"
    echo "$actual"
    echo "<<<"
fi

tear_down

exit "$fails"

