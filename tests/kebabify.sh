#!/bin/bash

dir=${BASH_SOURCE[0]%/*}

set -euo pipefail

. "$dir/../kebabify.sh"

dir=${BASH_SOURCE[0]%/*}

fails=0
i=0

while read line; do
    i=$((i + 1))
    expected="$(sed -ne "${i}p" "$dir/kebabify/data.parsed")"
    actual="$(kebabify "$line")"
    if test "$expected" != "$actual"; then
        fails=$((fails + 1))
        echo "$line: Failed"
        echo "EXPECTED OUTPUT: $expected"
        echo "ACTUAL OUTPUT: $actual"
    fi
done < "$dir/kebabify/data"

exit "$fails"

