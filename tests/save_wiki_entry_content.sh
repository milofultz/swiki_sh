#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/.test_setup_teardown.sh"
. "${BASH_SOURCE[0]%/*}/../save_wiki_entry_content.sh"

set_up

fails=0

for file in "${BASH_SOURCE[0]%/*}"/prepare_wiki_entry_content/*.md; do
    filename="$(basename "$file")"
    title="$(cat "${file%\.*}.title")"
    # CONTENT_PATH is from the init sourced in the save_wiki_entry_content file
    expected_destination="$CONTENT_PATH/$title"
    actual_destination="$(save_wiki_entry_content "$file")"
    if [ "$expected_destination" != "$actual_destination" ]; then
        fails=$((fails + 1))
        echo "$file: Destination file '$actual_destination' doesn't match expected '$expected_destination'."
    elif [ ! -e "$actual_destination" ]; then
        fails=$((fails + 1))
        echo "$file: Destination file '$actual_destination' not found"
    fi
done

tear_down

exit "$fails"

