#!/bin/bash

dir=${BASH_SOURCE[0]%/*}

set -euo pipefail

. "$dir/../replace_backlinks.sh"

dir=${BASH_SOURCE[0]%/*}

fails=0
i=0

for file in "$dir"/replace_backlinks/*.html ; do
    i=$((i + 1))
    expected="$(cat "${file%\.*}.parsed")"
    actual="$(replace_backlinks "$file")"
    if test "$expected" != "$actual"; then
        fails=$((fails + 1))
        echo "$file: Failed"
        echo "EXPECTED OUTPUT >>>"
        echo "$expected"
        echo "<<<"
        echo "ACTUAL OUTPUT >>>"
        echo "$actual"
        echo "<<<"
    fi
done

exit "$fails"

