#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/../prepare_wiki_entry_content.sh"

fails=0

for file in "${BASH_SOURCE[0]%/*}"/prepare_wiki_entry_content/*.md; do
    filename="$(basename "$file")"
    expected="$(cat "${BASH_SOURCE[0]%/*}/prepare_wiki_entry_content/${filename%\.*}.html")"
    actual="$(prepare_wiki_entry_content "$file")"
    if test "$expected" != "$actual" ; then
        fails=$((fails + 1))
        echo "$file: Failed"
        echo "EXPECTED OUTPUT >>>"
        echo "$expected"
        echo "<<<"
        echo "ACTUAL OUTPUT >>>"
        echo "$actual"
        echo "<<<"
    fi
done

exit "$fails"

