#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/.test_setup_teardown.sh"
. "${BASH_SOURCE[0]%/*}/../kebabify.sh"
. "${BASH_SOURCE[0]%/*}/../save_backlinks.sh"

fails=0

for file in "${BASH_SOURCE[0]%/*}"/save_backlinks/*.html; do
    set_up
    # Get title from name of input file, since a parsed wiki entry should be
    # named using the front matter anyway
    filename="$(basename "$file")"
    title="${filename%\.*}"
    save_backlinks "$file" | while read -r line; do
        destination_path="$BACKLINKS_PATH/$line"
        link_for_sed="<li><a href=\"$(kebabify "$title").html\">$title<\\/a><\\/li>"
        if [ ! -e "$destination_path" ]; then
            fails=$((fails + 1))
            echo "'$destination_path' does not exist, though it was linked to in source file."
        elif [ -z "$(sed -ne "/$link_for_sed/p" "$destination_path")" ]; then
            fails=$((fails + 1))
            echo "'$destination_path' does not contain '$link' as an existing backlink."
        fi
    done
    tear_down
done

# These files have no backlinks and should return nothing
for file in "${BASH_SOURCE[0]%/*}"/save_backlinks/none/*.html; do
    set_up
    # Get title from name of input file, since a parsed wiki entry should be
    # named using the front matter anyway
    filename="$(basename "$file")"
    title="${filename%\.*}"
    result=$(save_backlinks "$file")
    if [ -n "$result" ]; then
        fails=$((fails + 1))
        echo -e "'none/$file' should have no backlinks, but some were found:\n$result"
    fi
    tear_down
done

exit "$fails"

