#!/bin/bash

. "${BASH_SOURCE[0]%/*}/regex.sh"

#
# Scrapes all the properties from the YAML front matter and echoes the `title`
# and `description`, respectively.
#
# @param {String} $1:filepath The markdown file with YAML metadata.
# @returns {String} `title` and `description` separated by newlines.
#
get_metadata() {
    filepath="$1"
    awk $"
        function simpleTitle(file) {
            sub(/.*\\//, \"\", file);
            sub(/\\..*$/, \"\", file);
            return file;
        }

        BEGIN {
            FS = \":\"
            in_yaml = 0;
            title = \"\";
            description = \"\";
        }

        /^---$/ {
            in_yaml = !in_yaml;
            next;
        }

        // {
            if (!in_yaml) {
                next;
            }

            gsub($TRIM_SPACE_RE, \"\", \$2);

            if (\$1 == \"title\") {
                title = \$2;
            } else if (\$1 == \"description\") {
                description = \$2;
            }
        }

        END {
            if (length(title) == 0) {
                print simpleTitle(FILENAME);
            } else {
                print title;
            }
            print description;
        }
    " "$filepath"
}
