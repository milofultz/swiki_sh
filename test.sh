#!/bin/sh

# Set current directory to the current file
cd "${0%/*}" || exit 1

# Initialize variables
fail=0
tests=0
passed=0

# For each test in the "tests" folder,
for test in tests/*.sh ; do
    # Increment `tests`
    tests=$((tests+1))
    echo "TEST: $test"
    # If the test succeeded, increment `passed`
    if "./$test"; then
        echo "OK: ---- $test"
        passed=$((passed+1))
    # Else, increment `failed`
    else
        echo "FAIL: $test"
        fail=$((fail+ret))
    fi
done

# If no failures, return 0
if [ "$fail" -eq 0 ]; then
    echo 'SUCCESS'
    exitcode=0
# Else, return 1
else
    echo 'FAILURE '
    exitcode=1
fi

echo "$passed / $tests"
exit $exitcode

