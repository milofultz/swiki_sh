#!/bin/bash

set -euo pipefail

. "${BASH_SOURCE[0]%/*}/init.sh"
. "${BASH_SOURCE[0]%/*}/prepare_wiki_entry_content.sh"
. "${BASH_SOURCE[0]%/*}/get_metadata.sh"

#
# @param {String} $1:filepath Wiki entry to save in metadata folder
# @param {String} $2:input_dir The base of the input folder
# @returns {String} Path to saved wiki entry metadata, named with the
# kababified title.
#
save_metadata() {
    filepath="$(realpath "$1")"
    input_folder="$(realpath "${2:-$(dirname $1)}")"
    filename="${filepath##*/}"

    metadata=$(get_metadata "$filepath")
    title="$(echo "$metadata" | head -n1)"
    kebab_title="$(kebabify "$title")"

    # Get the folder relative to input folder and without leading slash
    relative_folder="${filepath//$input_folder/}"
    relative_folder="${relative_folder//\/$filename/}"
    relative_folder="${relative_folder:1}" # remove the leading slash if exists

    if test "$relative_folder" != ""; then
        mkdir -p "$METADATA_PATH/$relative_folder"
        destination_filepath="$METADATA_PATH/$relative_folder/$kebab_title"
    else
        destination_filepath="$METADATA_PATH/$kebab_title"
    fi

    echo "$metadata" > "$destination_filepath"
    echo "$destination_filepath"
}

