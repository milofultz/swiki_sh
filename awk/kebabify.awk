function kebabify(text) {
    newText = tolower(text);
    gsub(/ /, "-", newText);
    gsub(/[[\/()\'\".!?,\]]/, "", newText);
    return newText;
}

